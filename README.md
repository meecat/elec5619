# [ELEC5619 Doctor Online](https://bitbucket.org/meecat/elec5619/wiki/Home) #

Doctor Online is a positive computing project aimed at enhancing the efficiency and effectiveness of seeking health consultants via online medical consultation.

[Project Wiki](https://bitbucket.org/meecat/elec5619/wiki/Home)

## Run 

Right click project -> Run As ->Spring Boot App

## System Structure Diagram

![5619-HLD-Doctor.png](https://bitbucket.org/repo/akKkKLB/images/2204508341-5619-HLD-Doctor.png)


## MYSQL 

Online Management: [http://mysql.yopo.world/phpmyadmin](http://mysql.yopo.world/phpmyadmin)

user: meerkat

password: 
 


